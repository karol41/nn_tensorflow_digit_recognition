import tensorflow as tf
# from keras.models import load_model
import numpy as np

import os.path
import h5py

def get_digit_from_image(image_array):
  model = tf.keras.Sequential()
  file_name = 'my_model.h5'
  check = os.path.isfile(file_name)
  if(check == False):

    (train_images, train_labels), (test_images, test_labels) = tf.keras.datasets.mnist.load_data()

    TRAINING_SIZE = len(train_images)
    TEST_SIZE = len(test_images)

    # import random
    # import matplotlib.pyplot as plt
    #
    # # for _ in range(10):
    # #   i = random.randint(0, 100)
    # #   print("Label: %s" % train_labels[i])
    # #   plt.imshow(train_images[i])
    # #   plt.show()
    # #   testVar = input("Continue?")


    # Reshape from (N, 28, 28) to (N, 784)
    train_images = np.reshape(train_images, (TRAINING_SIZE, 784))
    test_images = np.reshape(test_images, (TEST_SIZE, 784))

    # Convert the array to float32 as opposed to uint8
    train_images = train_images.astype(np.float32)
    test_images = test_images.astype(np.float32)

    # Convert the pixel values from integers between 0 and 255 to floats between 0 and 1
    train_images /= 255
    test_images /= 255

    NUM_DIGITS = 10

    # print("Before", train_labels[0]) # The format of the labels before conversion

    train_labels  = tf.keras.utils.to_categorical(train_labels, NUM_DIGITS)

    # print("After", train_labels[0]) # The format of the labels after conversion

    test_labels = tf.keras.utils.to_categorical(test_labels, NUM_DIGITS)


    # model = tf.keras.Sequential()
    model.add(tf.keras.layers.Dense(512, activation=tf.nn.relu, input_shape=(784,)))
    model.add(tf.keras.layers.Dense(10, activation=tf.nn.softmax))

    # We will now compile and print out a summary of our model
    model.compile(loss='categorical_crossentropy',
                  optimizer='rmsprop',
                  metrics=['accuracy'])

    model.summary()

    model.fit(train_images, train_labels, epochs=5)
    loss, accuracy = model.evaluate(test_images, test_labels)
    print('Test accuracy: %.2f' % (accuracy))
    
    model.save('my_model.h5')
    print('The model has been saved.')
  else: 
    model = tf.keras.models.load_model('my_model.h5')

  # output_digit = model.predict_proba(image_array)
  output_digit = model.predict_classes(image_array)
  return output_digit

if __name__ == "__main__":

  img = [[0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,0,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,0,0,0,0,0,0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]]
  mat = np.array(img)
  # mat = mat / 255
  output_digit = get_digit_from_image(mat)
  print("Predicted digit from test data is: " +str(output_digit))

