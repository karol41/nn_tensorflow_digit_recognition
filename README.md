# Simple digit recognition in Python with Tensorflow

Based on:
[MNIST](http://yann.lecun.com/exdb/mnist/) data

[Tensorflow](https://github.com/tensorflow/workshops) workshops

<img src="./doc/gui.png" width="200">



Usage:

~~~~
python3 better_paint.py
~~~~

TODO:

* Preprocessing of the images
* Improve GUI
* Utilize CNN models

<!--
![alt text](./doc/gui.png)
The original black and white (bilevel) images from NIST were size normalized to fit in a 20x20 pixel box while preserving their aspect ratio. The resulting images contain grey levels as a result of the anti-aliasing technique used by the normalization algorithm. the images were centered in a 28x28 image by computing the center of mass of the pixels, and translating the image so as to position this point at the center of the 28x28 field.
The classification shown above uses some preprocessing of the drawn digit to make it more similar to the MNIST data: After drawing a digit in the above area (280x280 pixels), the drawing is centered on its center of mass, then rescaled to make its bounding box fit into a 200x200 pixels region (see the above link for instructions on how the original MNIST data was preprocessed). The image is then binned into a 28 x 28 representation and fed into the neural net.
Optionally (check the "Scale Stroke Width" checkbox), the digit is first redrawn with a scaled strokewidth to counteract the linewidth-changing effect of the subsequent rescaling. This can help with very small digits; the effect can be observed by checking the "Display Preprocessing" box.
http://myselph.de/neuralNet.html
-->