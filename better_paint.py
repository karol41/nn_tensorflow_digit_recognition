import tkinter as tk
from tkinter import Scale
import numpy as np
from scipy import ndimage

import PIL
from PIL import Image, ImageDraw, ImageGrab
from keras_mnist import get_digit_from_image


class ImageGenerator:
    def __init__(self,parent,posx,posy,*kwargs):
        self.parent = parent
        self.posx = posx
        self.posy = posy
        self.sizex = 280
        self.sizey = self.sizex
        self.b1 = "up"
        self.xold = None
        self.yold = None
        self.drawing_area=tk.Canvas(self.parent,width=self.sizex,height=self.sizey,bd=3,relief="ridge")
        self.drawing_area.place(x=self.posx,y=self.posy)
        self.drawing_area.bind("<Motion>", self.motion)
        self.drawing_area.bind("<ButtonPress-1>", self.b1down)
        self.drawing_area.bind("<ButtonRelease-1>", self.b1up)
        # self.button=tk.Button(self.parent,text="Check!",width=5,bg='white',command=self.save)
        self.label = tk.Label(parent)
        self.label.pack(side="bottom")
        self.button = tk.Button(self.parent, text="Recognize", width=8, bg='white', command=self.but_print)

        self.button.place(x=self.sizex//10,y=self.sizey+40)
        self.button1=tk.Button(self.parent,text="Clear!",width=5,bg='white',command=self.clear)
        self.button1.place(x=(self.sizex//10)+180,y=self.sizey+40)

        self.image=Image.new("RGB",(self.sizex//10, self.sizey//10),(255,255,255))
        self.draw=ImageDraw.Draw(self.image)
        self.digit = 99
        # slider
        slider1 = tk.Scale(root, from_=1, to=16, length=200, digits=0)  # type: Scale
        slider1.set(4)
        slider1.place(x=310, y=15)
        # w1.pack()
        self.slider1 = slider1

    # save image
    def save(self, filename="temp.jpg"):
        self.image.save(filename)

    def but_print(self):
        # self.image.show()
        # self.save()
        tmp_image = self.image.convert('L')
        # tmp_image.show()
        array = np.array(list(tmp_image.getdata())).reshape(28, 28)
        # array = np.array(list(tmp_image.getdata()))
        # array = self.block_mean(array, 10)
        array *= (-1)
        array += 255
        # array = array**3
        # array = np.array(array, dtype=np.uint8)
        # debug
        # from matplotlib import pyplot as plt
        # plt.imshow(array, interpolation='nearest')
        # plt.show(block=False)

        array = array.reshape(28*28)
        array = np.expand_dims(array, axis=0)
        # print(array)
        var = get_digit_from_image(array)
        # print(var)
        comm = "Digit predict: " + str(var)
        self.label.config(text=comm)

    def block_mean(self, ar, fact):
        # assert isinstance(fact, int), type(fact)
        sx, sy = ar.shape
        X, Y = np.ogrid[0:sx, 0:sy]
        regions = sy // fact * (X // fact) + Y // fact
        res = ndimage.mean(ar, labels=regions, index=np.arange(regions.max() + 1))
        res.shape = (sx // fact, sy // fact)
        return res

    def clear(self):
        self.drawing_area.delete("all")
        self.image = Image.new("RGB",(self.sizex//10, self.sizey//10),(255,255,255))
        self.draw = ImageDraw.Draw(self.image)
        self.label.config(text=[])

    def b1down(self,event):
        self.b1 = "down"

    def b1up(self,event):
        self.b1 = "up"
        self.xold = None
        self.yold = None

    def motion(self,event):
        if self.b1 == "down":
            if self.xold is not None and self.yold is not None:
                ra = self.slider1.get() * 2
                event.widget.create_line(self.xold,self.yold,event.x,event.y,smooth='true',width=ra,fill='blue',capstyle='round', splinesteps=36)
                # event.widget.create_oval(self.xold - ra , self.yold - ra, self.xold +ra , self.yold +ra, width=ra*2, fill='black')
                self.draw.line(((self.xold//10,self.yold//10),(event.x//10,event.y//10)),(0,0,0),width=ra//10)

        self.xold = event.x
        self.yold = event.y

    # def getImage(self, widget):
    #     x = root.winfo_rootx() + widget.winfo_x()
    #     y = root.winfo_rooty() + widget.winfo_y()
    #     x1 = x + widget.winfo_width()
    #     y1 = y + widget.winfo_height()
    #     im = ImageGrab.grab().crop((x, y, x1, y1))
    #     return im.convert('L')

if __name__ == "__main__":
    root=tk.Tk()
    root.wm_geometry("%dx%d+%d+%d" % (400, 400, 10, 10))
    root.config(bg='white')
    ImageGenerator(root,10,10)
    root.mainloop()